-----------------------------------------------------------------------------
-- |
-- Module      : System.Environment.XDG.DesktopEntry
-- Copyright   : 2019 Ivan Malison
-- License     : BSD3-style (see LICENSE)
--
-- Maintainer  : Ivan Malison
-- Stability   : unstable
-- Portability : unportable
--
-- Implementation of version 1.2 of the freedesktop "Desktop Entry
-- specification", see
-- https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-1.2.html.
-----------------------------------------------------------------------------

module System.Environment.XDG.DesktopEntry
  ( DesktopEntry(..)
  , command
  , comment
  , hasCategory
  , icon
  , name
  , noDisplay
  , notShowIn
  , onlyShowIn
  , getClassNames
  , getDirectoryEntriesDefault
  , getDirectoryEntry
  , getDirectoryEntryDefault
  , getXDGDataDirs
  , indexDesktopEntriesBy
  , indexDesktopEntriesByClassName
  , listDesktopEntries
  , readDesktopEntry
  ) where

import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Except
import           Data.Char
import qualified Data.ConfigFile as CF
import           Data.Either
import           Data.Either.Combinators
import qualified Data.MultiMap as MM
import           Data.List
import           Data.Maybe
import           Safe
import           System.Directory
import           System.FilePath.Posix
import           System.Posix.Files
import           Text.Printf
import           Text.Read (readMaybe)

data DesktopEntryType = Application | Link | Directory
  deriving (Read, Show, Eq)

-- | Get all of the XDG data directories (both global and user).
getXDGDataDirs :: IO [FilePath]
getXDGDataDirs =
  liftM2 (:) (getXdgDirectory XdgData "") (getXdgDirectoryList XdgDataDirs)

-- | Desktop Entry. All attributes (key-value-pairs) are stored in an
-- association list.
data DesktopEntry = DesktopEntry
  { entryType :: DesktopEntryType
  , filename :: FilePath -- ^ unqualified filename, e.g. "firefox.desktop"
  , attributes :: [(String, String)] -- ^ Key-value pairs
  } deriving (Read, Show, Eq)

-- | Determine whether the Category attribute of a desktop entry contains a
-- given value.
hasCategory
  :: DesktopEntry
  -> String
  -> Bool
hasCategory de cat =
  maybe False ((cat `elem`) . splitAtSemicolon) $
        lookup "Categories" (attributes de)

splitAtSemicolon :: String -> [String]
splitAtSemicolon = lines . map (\c -> if c == ';' then '\n' else c)

-- | Return the proper name of the desktop entry, depending on the list of
-- preferred languages.
name
  :: [String] -- ^ Preferred languages
  -> DesktopEntry
  -> String
name langs de = fromMaybe (filename de) $ localisedAtt langs de "Name"

-- | Return the categories in which the entry shall be shown
onlyShowIn :: DesktopEntry -> [String]
onlyShowIn = maybe [] splitAtSemicolon . att "OnlyShowIn"

-- | Return the categories in which the entry shall not be shown
notShowIn :: DesktopEntry -> [String]
notShowIn = maybe [] splitAtSemicolon . att "NotShowIn"

-- | Return the value of the given attribute key
att :: String -> DesktopEntry -> Maybe String
att att = lookup att . attributes

-- | Return the Icon attribute
icon :: DesktopEntry -> Maybe String
icon = att "Icon"

-- | Return True if the entry must not be displayed
noDisplay :: DesktopEntry -> Bool
noDisplay de = maybe False (("true" ==) . map toLower) $ att "NoDisplay" de

localisedAtt
  :: [String] -- ^ Preferred languages
  -> DesktopEntry
  -> String
  -> Maybe String
localisedAtt langs de att =
  let localeMatches =
        mapMaybe (\l -> lookup (att ++ "[" ++ l ++ "]") (attributes de)) langs
  in if null localeMatches
       then lookup att $ attributes de
       else Just $ head localeMatches

-- | Return the proper comment of the desktop entry, depending on the list of
-- preferred languages.
comment :: [String] -- ^ Preferred languages
          -> DesktopEntry
          -> Maybe String
comment langs de = localisedAtt langs de "Comment"

-- | Return the command that should be executed when running this desktop entry.
command :: DesktopEntry -> Maybe String
command de =
  reverse . dropWhile (== ' ') . reverse . takeWhile (/= '%') <$>
  lookup "Exec" (attributes de)

-- | Return a list of all desktop entries in the given directory.
listDesktopEntries
  :: String -- ^ The extension to use in the search
  -> FilePath -- ^ The filepath at which to search
  -> IO [DesktopEntry]
listDesktopEntries extension dir = do
  let normalizedDir = normalise dir
  ex <- doesDirectoryExist normalizedDir
  if ex
  then do
    files <- map (normalizedDir </>) <$> listDirectory dir
    entries <-
      (nub . rights) <$>
      mapM readDesktopEntry (filter (extension `isSuffixOf`) files)
    subDirs <- filterM doesDirectoryExist files
    subEntries <- concat <$> mapM (listDesktopEntries extension) subDirs
    return $ entries ++ subEntries
  else return []

-- XXX: This function doesn't recurse, but `listDesktopEntries` does. Why?
-- Shouldn't they really share logic...
-- | Retrieve a desktop entry with a specific name.
getDirectoryEntry :: [FilePath] -> String -> IO (Maybe DesktopEntry)
getDirectoryEntry dirs name = do
  exFiles <- filterM doesFileExist $ map ((</> name) . normalise) dirs
  join . (fmap rightToMaybe) <$> traverse readDesktopEntry (headMay exFiles)

-- | Get a desktop entry with a specific name from the default directory entry
-- locations.
getDirectoryEntryDefault :: String -> IO (Maybe DesktopEntry)
getDirectoryEntryDefault entry =
  fmap (</> "applications") <$> getXDGDataDirs >>=
  flip getDirectoryEntry (printf "%s.desktop" entry)

-- | Get all instances of 'DesktopEntry' for all desktop entry files that can be
-- found by looking in the directories specified by the XDG specification.
getDirectoryEntriesDefault :: IO [DesktopEntry]
getDirectoryEntriesDefault =
  fmap (</> "applications") <$> getXDGDataDirs >>= foldM addDesktopEntries []
  where addDesktopEntries soFar directory =
          (soFar ++) <$> listDesktopEntries "desktop" directory

-- | Read a desktop entry from a file.
readDesktopEntry :: FilePath -> IO (Either (CF.CPErrorData, String) DesktopEntry)
readDesktopEntry filePath = runExceptT $ do
  result <- (join $ liftIO $ CF.readfile CF.emptyCP filePath) >>=
            flip CF.items "Desktop Entry"
  return DesktopEntry
         { entryType = fromMaybe Application $ lookup "Type" result >>= readMaybe
         , filename = filePath
         , attributes = result
         }

-- | Construct a 'MM.Multimap' where each 'DesktopEntry' in the provided
-- foldable is indexed by the keys returned from the provided indexing function.
indexDesktopEntriesBy ::
  Foldable t => (DesktopEntry -> [String]) ->
  t DesktopEntry -> MM.MultiMap String DesktopEntry
indexDesktopEntriesBy getIndices = foldl insertByIndices MM.empty
  where
    insertByIndices entriesMap entry =
      foldl insertForKey entriesMap $ getIndices entry
        where insertForKey innerMap key = MM.insert key entry innerMap

-- | Get all the text elements that could be interpreted as class names from a
-- 'DesktopEntry'.
getClassNames :: DesktopEntry -> [String]
getClassNames DesktopEntry { attributes = attributes, filename = filepath } =
  (snd $ splitExtensions $ snd $ splitFileName filepath) :
  catMaybes [lookup "StartupWMClass" attributes, lookup "Name" attributes]

-- | Construct a multimap where desktop entries are indexed by their class
-- names.
indexDesktopEntriesByClassName
  :: Foldable t => t DesktopEntry -> MM.MultiMap String DesktopEntry
indexDesktopEntriesByClassName = indexDesktopEntriesBy getClassNames
